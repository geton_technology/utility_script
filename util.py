import requests
import os.path
from os import path
import sys
# import pathlib
import time


headers = {
    'PRIVATE-TOKEN': "xxxxx",
    'cache-control': "no-cache",
}

# def LoopCheck(timeout,run_funtion):
#     timeout = time.time() + 60*timeout   # 5 minutes from now
#     while True:
#         test = 0
#         if test == 5 or time.time() > timeout:
#             break
#         run_funtion()
#         time.sleep(10)
#         test = test - 1
def test_print():
    print("from function test_print().....")


def getLatestJob(project_id,branch,job_name):
    url = "https://gitlab.com/api/v4/projects/{0}/jobs?scope=success".format(project_id)
    try:
        response = requests.request("GET", url, headers=headers)
        data = response.json()
        res = []
        for x in data:
            # print(x['name'],x['ref']) #debug
            if x['name'] == job_name and x['ref'] == branch:
                res.append(x)
        print(res[0]['id'])
        return res[0]['id']
    except:
        response = requests.request("GET", url, headers=headers)
        print("url: {0}, Http status: {1}".format(url,response.status_code))
        print("Headers : {0}".format(headers))
        print("Http Response: {0} ".format(response.text))
        sys.exit(1)



def runLatestJob(project_id,branch,job_name):
    job_id = getLatestJob(project_id,branch,job_name)
    url = "https://gitlab.com/api/v4/projects/{0}/jobs/{1}/retry"\
        .format(project_id,job_id)
    response = requests.request("POST", url, headers=headers)
    data= response.json()
    new_job_ID = data['id']
    print(new_job_ID)
    get_status_job_url = "https://gitlab.com/api/v4/projects/{0}/jobs/{1}".format(project_id,new_job_ID)
    while True:
        n = 1
        if n == 60:
            print("get job status timeout")
            sys.exit(1)
            break
        n = n + 1
        res_text = requests.request("GET",url=get_status_job_url,headers=headers)
        res_text = res_text.json()
        if res_text['status'] == "success":
            print("this job is success ")
            break
        time.sleep(10)
        print("job still running.......Ref url: {0}".format(get_status_job_url))

if __name__ == '__main__':
    try:
        private_token = os.getenv("PRIVATE_TOKEN")
        headers = {
            'PRIVATE-TOKEN': private_token,
            'cache-control': "no-cache",
        }
    except:
        print("can't get gitlab private token from environment variable, Please set env export PRIVATE_TOKEN='xxxxxxx' first")
        sys.exit(1)
    num_arg = len(sys.argv)
    if num_arg == 2:
        print('function only')
        globals()[sys.argv[1]]()
    elif num_arg ==3:
        print("function and one arg")
        globals()[sys.argv[1]](sys.argv[2])
    elif num_arg ==5:
        globals()[sys.argv[1]](sys.argv[2],sys.argv[3],sys.argv[4])
    else:
        print("xxx")
        os.exit(1)
